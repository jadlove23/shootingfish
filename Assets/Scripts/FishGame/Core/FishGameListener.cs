public interface IFishGameUIListener
{
  void AdjustBet(int operation);
  void ChangeTurret(int index);
}

public class FishGameUIListener : IFishGameUIListener
{

  public void AdjustBet(int operation)
  {
    Game.GetInstance().GetFishRoom().AdjustBet(operation);
  }

  public void ChangeTurret(int index)
  {
    Game.GetInstance().GetFishRoom().ChangeTurret(index);
  }
}