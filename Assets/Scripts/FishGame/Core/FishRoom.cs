using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FishGame
{
  public class FishRoomListener
  {
    public delegate void OnRoomInit(PlayerDetail[] players, uint roomTime, uint currentFishID, uint[] destroyed);
    public OnRoomInit onRoomInit;
    public delegate void OnSendFishList(FishInitData[] fishes);
    public OnSendFishList onSendFishList;
    public delegate void OnPlayerFire(uint seat, uint bullet_type, uint bet, uint angle);
    public OnPlayerFire onPlayerFire;
    public delegate void OnPlayerFireLock(uint seat, uint bullet_type, uint bet, uint fishId);
    public OnPlayerFireLock onPlayerFireLock;
    public delegate void OnDestroyFish(uint seat, uint fishId, RewardAmount reward);
    public OnDestroyFish onDestroyFish;
    public delegate void OnUpdatePlayerChip(uint seat, ulong chip);
    public OnUpdatePlayerChip onUpdatePlayerChip;
    public delegate void OnPlayerJoin(uint seat, PlayerDetail player);
    public OnPlayerJoin onPlayerJoin;
    public delegate void OnPlayerLeave(uint seat);
    public OnPlayerLeave onPlayerLeave;

    public delegate void OnAdjustBet(int seat, uint bet, TurretBetState state);
    public OnAdjustBet onAdjustBet;

    public delegate void OnChangTurretData(int seat, int index);
    public OnChangTurretData onChangTurretData;
  }

  public class FishRoom
  {
    private uint _RoomId;
    private uint _Timelapse = 0;
    private uint _ClientId;
    private int _ClientSeat = -1;
    private int _ClinetTurretIndex = 1;
    private readonly PlayerDetail[] _Players;
    private HashSet<uint> _DestroyFishId;

    private TurretBetHandle _TurretBetHandle = new TurretBetHandle();
    private DelegateReceiver _Receiver;
    private FishRoomListener _Listener;

    public FishRoomListener GetListener() { return _Listener; }

    private readonly uint[] _Bets;

    private const int MAX_PLAYER = 4;
    public static readonly PlayerDetail EMPTY_PLAYER = new PlayerDetail() { user_id = 0, chip = 0 };

    public FishRoom(uint roomId, uint[] bets, uint clientId)
    {
      _RoomId = roomId;
      _Bets = bets;
      _ClientId = clientId;
      _Listener = new FishRoomListener();
      _Players = InitPlayerDetails();
    }

    public void SetupReceiver(DelegateReceiver receiver)
    {
      _Receiver = receiver;
      _Receiver.OnFishGameRoomSetup += InitRoom;
      _Receiver.OnFishGamePlayerLeave += PlayerLeave;
      _Receiver.OnFishGamePlayerJoin += PlayerJoin;
      _Receiver.OnPlayerFire += PlayerFire;
      _Receiver.OnPlayerFireLock += PlayerFireLock;
      _Receiver.OnFishList += RecvFishList;
      _Receiver.OnDestroyFish += RecvDestroyFish;
      _Receiver.OnUpdateFishGamePlayerChip += RecvUpdatePlayerChip;
    }

    public void Cleanup()
    {
      _Receiver.OnFishGameRoomSetup -= InitRoom;
      _Receiver.OnFishGamePlayerLeave -= PlayerLeave;
      _Receiver.OnFishGamePlayerJoin -= PlayerJoin;
      _Receiver.OnPlayerFire -= PlayerFire;
      _Receiver.OnPlayerFireLock -= PlayerFireLock;
      _Receiver.OnFishList -= RecvFishList;
      _Receiver.OnDestroyFish -= RecvDestroyFish;
      _Receiver.OnUpdateFishGamePlayerChip -= RecvUpdatePlayerChip;
      _Receiver = null;
    }

    // from server

    private void InitRoom(PlayerDetail[] players, uint room_t, uint fish_id, FishInitData[] fish_list, uint[] destroyed)
    {
      _Timelapse = room_t;
      _DestroyFishId = new HashSet<uint>(destroyed);
      SetupPlayers(players);
      _Listener.onRoomInit?.Invoke(players, room_t, fish_id, destroyed);
      _TurretBetHandle.UpdateChip(players[_ClientSeat].chip);
      PreferBet(players[_ClientSeat].chip);
      _Listener.onSendFishList?.Invoke(fish_list);
    }

    private void PlayerLeave(uint seat)
    {
      if (!IsValdSeat(seat)) return;
      _Players[seat] = null;
      _Listener.onPlayerLeave?.Invoke(seat);
    }

    private void PlayerJoin(uint seat, PlayerDetail player)
    {
      if (!IsValdSeat(seat)) return;
      _Players[seat] = player;
      _Listener.onPlayerJoin?.Invoke(seat, player);
    }

    private void PlayerFire(uint seat, uint bullet_type, uint bet, uint angle)
    {
      if (!IsValdSeat(seat)) return;
      _Listener.onPlayerFire?.Invoke(seat, bullet_type, bet, angle);
    }

    private void PlayerFireLock(uint seat, uint bullet_type, uint bet, uint fish_id)
    {
      if (!IsValdSeat(seat)) return;
      _Listener.onPlayerFireLock?.Invoke(seat, bullet_type, bet, fish_id);
    }

    private void RecvFishList(FishInitData[] fishes)
    {
      _Listener.onSendFishList?.Invoke(fishes);
    }

    private void RecvUpdatePlayerChip(uint seat, ulong chip)
    {
      UpdatePlayerChip(seat, chip, false);
    }

    private void RecvDestroyFish(uint fish_id, uint seat, RewardAmount reward)
    {
      if (!IsValdSeat(seat)) return;
      if (_DestroyFishId.Contains(fish_id))
      {
        Debug.LogWarning($"Fish id {fish_id} has already be destroyed.");
        return;
      }
      _DestroyFishId.Add(fish_id);
      _Listener.onDestroyFish?.Invoke(seat, fish_id, reward); // client increase chip from reward amount here
      _Players[seat].chip += reward.amount;
      UpdatePlayerChip(seat, _Players[seat].chip, true);
    }

    // usage method

    private bool IsValdSeat(uint seat)
    {
      return seat < _Players.Length;
    }

    private bool IsClientSeat(uint seat)
    {
      if (!IsValdSeat(seat)) return false;
      return _Players[(int)seat].user_id == _ClientId;
    }

    private PlayerDetail[] InitPlayerDetails()
    {
      PlayerDetail[] players = new PlayerDetail[MAX_PLAYER];
      for (int i = 0; i < MAX_PLAYER; i++)
      {
        players[i] = EMPTY_PLAYER;
      }
      return players;
    }

    private void SetupPlayers(PlayerDetail[] players)
    {
      for (int i = 0; i < MAX_PLAYER; i++)
      {
        _Players[i] = new PlayerDetail() { user_id = players[i].user_id, chip = players[i].chip };
        if (_ClientId == _Players[i].user_id)
        {
          _ClientSeat = i;
        }
      }
    }

    private void UpdatePlayerChip(uint seat, ulong chip, bool sendClient)
    {
      if (!IsValdSeat(seat)) return;
      _Players[seat].chip = chip;
      _TurretBetHandle.UpdateChip(chip);
      if (!sendClient && IsClientSeat(seat)) return;
      _Listener?.onUpdatePlayerChip(seat, chip);
    }

    public void AdjustBet(int opoperation)
    {
      _TurretBetHandle.CalculateAdjustBet(opoperation);
      _Listener.onAdjustBet?.Invoke(_ClientSeat, (uint)_TurretBetHandle.Getbet(), _TurretBetHandle.GetTurretBetState());
    }

    public void PreferBet(ulong chip)
    {
      _TurretBetHandle.CalculatePreferBet(chip);
      _Listener.onAdjustBet?.Invoke(_ClientSeat, (uint)_TurretBetHandle.Getbet(), _TurretBetHandle.GetTurretBetState());
    }

    public void ChangeTurret(int index)
    {
      _ClinetTurretIndex = index;
      _ClinetTurretIndex = Mathf.Max(0, _ClinetTurretIndex);
      _Listener.onChangTurretData?.Invoke(_ClientSeat, _ClinetTurretIndex);
    }

    public bool CheckClientFire(uint bet)
    {
      if (_ClientSeat < 0) return false;
      return _Players[_ClientSeat].chip >= bet;
    }

    public bool ClientFire()
    {
      if (_ClientSeat < 0) return false;
      if (!CheckClientFire((uint)_TurretBetHandle.Getbet())) return false;
      _Players[_ClientSeat].chip -= (uint)_TurretBetHandle.Getbet();
      _Listener?.onUpdatePlayerChip((uint)_ClientSeat, _Players[_ClientSeat].chip);
      return true;
    }
  }
}
