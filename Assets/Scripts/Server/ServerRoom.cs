using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using FishGame;

public class ServerRoom
{
  private bool _IsPlaying = false;
  private uint _Timer = 0;
  private uint _CurrentFishID = 0;
  private uint _FirstSendFishID = 0;
  private uint _SendFishCount = 1;
  private FishInitData[] _Fishes;

  private readonly HashSet<uint> _DestroyFishId = new HashSet<uint>();
  private readonly uint _RoomId;
  private readonly PlayerDetail[] _Players = new PlayerDetail[4];

  private Dictionary<uint, TypeOfFish> _FishTypeByID = new Dictionary<uint, TypeOfFish>();
  private readonly Dictionary<TypeOfFish, FishHitRate> _FishHitRate = new Dictionary<TypeOfFish, FishHitRate>();

  public const int MAX_PLAYER_SEAT = 4;

  public ServerRoom(uint roomId, FishInitData[] fishes, FishHitRate[] fishHits)
  {
    _RoomId = roomId;
    _Fishes = fishes;
    for (int i = 0; i < MAX_PLAYER_SEAT; i++)
    {
      _Players[i] = FishGame.FishRoom.EMPTY_PLAYER;
    }

    foreach (var fish in fishHits)
    {
      _FishHitRate.Add(fish.type, fish);
    }
  }

  public void PlayerJoin(uint seat, PlayerDetail player)
  {
    if (!IsValidSeat(seat)) return;
    _Players[seat] = player;
    if (CountPlayingPlayer() > 0) StartGame();
    Packet.SendFishGameScene(_RoomId, new uint[1]);
  }

  public void PlayerJoinSuccess()
  {
    Packet.SendFishGameRoomSetup(_Players, _Timer, _FirstSendFishID, _Fishes, _DestroyFishId.ToArray());
  }

  public void PlayerLeave(uint seat)
  {
    if (!IsValidSeat(seat)) return;
    _Players[seat] = FishGame.FishRoom.EMPTY_PLAYER;
    if (CountPlayingPlayer() == 0) ResetGame();
  }

  public void PlayerFire(uint seat, uint bet)
  {
    if (!IsValidSeat(seat)) return;
    if (_Players[seat].chip - bet < 0) return;
    _Players[seat].chip -= bet;
    Packet.SendPlayerUpdateChip(seat, _Players[seat].chip);
  }

  public void HitFish(uint seat, uint fishId, uint bet)
  {
    if (!IsValidSeat(seat)) return;
    if (!CheckDestroyFish(fishId)) return;
    TypeOfFish type = _FishTypeByID[fishId];
    uint rewardAmount = (uint)Mathf.Round(bet * _FishHitRate[type].reward);
    RewardAmount reward = new RewardAmount() { reward = "chip", amount = rewardAmount };
    _Players[seat].chip += reward.amount;
    _DestroyFishId.Add(fishId);
    Packet.SendFishDestroyID(fishId, seat, reward);
    Packet.SendPlayerUpdateChip(seat, _Players[seat].chip);
  }

  public void SendFish()
  {
    if (!_IsPlaying) return;
    List<FishInitData> nextFishes = new List<FishInitData>();
    foreach (var fish in _Fishes)
    { // test
      FishInitData newFish = new FishInitData()
      {
        fish_type = fish.fish_type,
        pattern = fish.pattern,
        x_offset = fish.x_offset,
        y_offset = fish.y_offset,
        init_t = fish.init_t + 10000 * _SendFishCount,
        life_time = fish.life_time,
      };
      nextFishes.Add(newFish);
    }

    SetupFish(nextFishes);

    _SendFishCount++;
    Packet.SendFishList(nextFishes.ToArray());
  }

  private bool IsValidSeat(uint seat)
  {
    return seat < _Players.Length;
  }

  private void StartGame()
  {
    if (_IsPlaying) return;
    _IsPlaying = true;
    SetupFish(_Fishes);
  }

  private void ResetGame()
  {
    if (!_IsPlaying) return;
    _IsPlaying = false;
    _CurrentFishID = 0;
    _Timer = 0;
    _FishTypeByID.Clear();
    _DestroyFishId.Clear();
    _SendFishCount = 1;
  }

  private void SetupFish(IEnumerable<FishInitData> fishes)
  {
    _FirstSendFishID = _CurrentFishID;

    foreach (var initFish in fishes)
    {
      FishSpawnData data = FishConst.FISH_SPAWN_DATA[initFish.fish_type];
      for (int i = 0; i < data.amount; i++)
      {
        uint fishID = _CurrentFishID++;
        TypeOfFish type = FishConst.FISH_SPAWN_DATA[initFish.fish_type].type;
        _FishTypeByID.Add(fishID, type);
      }
    }
  }

  private int CountPlayingPlayer()
  {
    return _Players.Count((e) => e.user_id > 0);
  }

  private bool CheckDestroyFish(uint fishId)
  {
    if (!_FishTypeByID.ContainsKey(fishId)) return false;
    if (_DestroyFishId.Contains(fishId)) return false;
    TypeOfFish type = _FishTypeByID[fishId];
    if (!_FishHitRate.ContainsKey(type)) return false;
    float rand = Random.Range(0f, 100f);
    Debug.Log($"Hit fish {fishId} type {type} get rand {rand} and hit rate is {_FishHitRate[type].hitRate}");
    return rand <= _FishHitRate[type].hitRate;
  }
}
