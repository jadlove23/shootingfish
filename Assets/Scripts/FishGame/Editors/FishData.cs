﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FishGame
{
  [CreateAssetMenu(fileName = "FishData", menuName = "ScriptableOjects/FishScriptableObjects", order = 1)]
  public class FishData : ScriptableObject
  {
    [SerializeField]
    public TypeOfFish Tag;
  }
}