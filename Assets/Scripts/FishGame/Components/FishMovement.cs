﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FishGame
{
  [RequireComponent(typeof(Animator))]
  public class FishMovement : MonoBehaviour
  {
    [SerializeField]
    private Animator _Animator;

    private int _Pattern;
    private float _ElapsedTime;
    private float _LifeTime = 5;

    private const float ANIM_TIME = 1f;
    private const float MS_PER_SECOND = 1000;

    public void Move(System.Action onEnd)
    {
      string animPlay = "FishMovement_" + _Pattern.ToString();
      float speed = (ANIM_TIME / _LifeTime) * MS_PER_SECOND; // from ms
      _Animator.SetFloat("speed", speed);
      _Animator.Play(animPlay, 0, Mathf.Clamp01(_ElapsedTime / _LifeTime));
      GetComponent<WarningObject>().SetEndEvent(onEnd);
    }

    public void SetMovementPattern(int pattern = 0)
    {
      int animationLength = _Animator.runtimeAnimatorController.animationClips.Length - 1;// minus idle state
      _Pattern = pattern % animationLength;
    }

    public float GetTimeToMove()
    {
      return _LifeTime;
    }

    public void SetCurrentTime(float elapseTime, float lifeTime)
    {
      _ElapsedTime = elapseTime;
      _LifeTime = lifeTime;
    }
  }
}