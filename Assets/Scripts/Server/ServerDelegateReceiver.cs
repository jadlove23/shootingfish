﻿using FishGame;
public class ServerDelegateReceiver
{
  public delegate void RecvPlayerUpdate(string roomID,PlayerData player);
  public RecvPlayerUpdate OnRecvPlayerUpdate;
  public delegate void RecvFishID(string id, uint roomID);
  public RecvFishID OnRecvFishDestroy;
  public delegate void RecvPlayerSit(string id, uint seat);
  public RecvPlayerSit OnRecvPlayerSit;
  public delegate void RecvPlayerLeaveRoom(RoomData room, uint seat);
  public RecvPlayerLeaveRoom OnRecvPlayerLeaveRoom;
  public delegate void RecvOnSceneLobby();
  public RecvOnSceneLobby OnRecvOnSceneLobby;

  public delegate void RecvJoinFishGame(uint index);
  public RecvJoinFishGame OnRecvJoinFishGame;
  public delegate void RecvFishGameLeave();
  public RecvFishGameLeave OnRecvFishGameLeave;
  public delegate void RecvPlayerFire(uint bulletType, uint bet, uint angle);
  public RecvPlayerFire OnRecvPlayerFire;
  public delegate void RecvPlayerFireLock(uint bulletType, uint bet, uint fishId);
  public RecvPlayerFire OnRecvPlayerFireLock;
  public delegate void RecvHitFish(uint fishId, uint bet);
  public RecvHitFish OnRecvHitFish;
}