﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FishGame;
using UnityEngine.SceneManagement;

public class Game : MonoBehaviour
{
  private static Game _Instance = null;
  public static Game GetInstance() { return _Instance; }

  private  DelegateReceiver  _Receiver = new DelegateReceiver ();
  public DelegateReceiver  GetReceiver() { return _Receiver; }

  private Lobby _LobbyData = new Lobby();
  public Lobby GetLobbyData() { return _LobbyData; }

  private FishRoom _Room;
  public FishRoom GetFishRoom() { return _Room; }

  public uint _UserID = 1;

  private ulong _Chip = 0;
  public ulong GetChip() { return _Chip; }
  
  private void Awake()
  {
    if (_Instance == null || _Instance == this)
    {
      _Instance = this;
      DontDestroyOnLoad(this);
    }
    else
    {
      Destroy(this.gameObject);
    }
    PreSetup();
    SetupReceiver();
  }

  private void SetupReceiver()
  {
    _Receiver.OnLobbyScene += RecvSceneLobby;
    _Receiver.OnSceneFishGame += RecvSceneFishGame;
    _Receiver.OnUpdateChip += RecvUpdateChip;
  }

  private void PreSetup() 
  {
    Packet.Setup(_Receiver);
    SoundManager.LoadAllAudioFromResource();
  }
  public void Cleanup()
  {

  }

  private void RecvSceneLobby()
  {
    _Room?.Cleanup();
    _Room = null;
    SceneManager.LoadScene("Lobby");
  }

  private void RecvSceneFishGame(uint roomId, uint[] bets)
  {
    var op = SceneManager.LoadSceneAsync("MainGame");
    _Room = new FishRoom(roomId, bets, 1);
    _Room.SetupReceiver(_Receiver);
    op.completed += (e) =>
    {
      MainGameController scene = GetGameScene();
      if (scene != null)
      {
        scene.SetupListener();
      }
      Server.GetInstance().PlayerJoinSuccess();
    };
  }

  private void RecvUpdateChip(ulong chip)
  {
    _Chip = chip;
  }

  private MainGameController GetGameScene()
  {
    return FindObjectOfType<MainGameController>();
  }
}