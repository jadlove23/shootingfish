﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace FishGame
{
  public class MainGameUI : MonoBehaviour
  {
    [SerializeField]
    private Text _ShowTime;
    [SerializeField]
    private Text _TotalTxt;
    [SerializeField]
    private Button _LeaveButton;
    [SerializeField]
    private Button _ChangeTurretButton;

    private float _CurrentTime = 0;
    private PlayerData _Player;

    private const string CHANGE_TURRET_PATH = "ChangeTurret";
    private void Start()
    {
      _LeaveButton.onClick.AddListener(LeaveGame);
    }
    public void SetupChangeTurretButton(IFishGameUIListener fishGameUIListener)
    {
      _ChangeTurretButton.onClick.AddListener(() => OpenUIChangeTurret(fishGameUIListener));
    }
    private void OpenUIChangeTurret(IFishGameUIListener fishGameUIListener)
    {
      GameObject obj = GameObjectFactory.InstantiateUI(CHANGE_TURRET_PATH, CHANGE_TURRET_PATH);
      ChangeTurretUI changeTurret = obj.GetComponentInChildren<ChangeTurretUI>();
      changeTurret.Setup(fishGameUIListener);
    }

    void Update()
    {
      _CurrentTime += Time.deltaTime;
      _ShowTime.text = _CurrentTime.ToString("F2");
      _TotalTxt.text = _Player.Money + "$";
    }

    private void LeaveGame()
    {
      NetworkHelper.SendFishGameLeave();
    }
  }
}
