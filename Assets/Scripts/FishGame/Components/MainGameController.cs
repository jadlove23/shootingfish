﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FishGame
{
  public class MainGameController : MonoBehaviour
  {
    private uint _CurrentTime;
    private SpawnManager _SpawnManager = new SpawnManager(); // spawn fish in scene
    private TurretPosition _TurretPosition; // show turret in player seat
    private MainGameUI _MainGameUI;

    private FishRoomListener _Listener;
    private IFishGameUIListener _FishGameUIListener;

    private float _Timer = 0f;
    private const float MAX_TIME_TO_TICK = 0.3f;
    private const uint UPDATE_TIME_AT_MS = 300;
    private const uint RESET_ROOM_T = 300000; // 5 minutes

    public void Awake()
    {
      _TurretPosition = GameObject.FindObjectOfType<TurretPosition>();
      _MainGameUI = GameObject.FindObjectOfType<MainGameUI>();
      _FishGameUIListener = new FishGameUIListener();
      SoundManager.PlayBGM(SoundManager.BGM.Maingame);
      //SetupListener();
    }

    public void SetupListener()
    {
      _Listener = Game.GetInstance().GetFishRoom().GetListener();
      _Listener.onRoomInit += RoomInit;
      _Listener.onSendFishList += RecvFishList;
      _Listener.onDestroyFish += RecvDestroyFish;
      _Listener.onAdjustBet += RecvAdjustBet;
      _Listener.onChangTurretData += RecvChangeTurret;
      _Listener.onUpdatePlayerChip += RecvUpdatePlayerChip;
    }

    private void Cleanup()
    {
      _Listener.onRoomInit -= RoomInit;
      _Listener.onSendFishList -= RecvFishList;
      _Listener.onDestroyFish -= RecvDestroyFish;
      _Listener.onAdjustBet -= RecvAdjustBet;
      _Listener.onChangTurretData -= RecvChangeTurret;
      _Listener.onUpdatePlayerChip -= RecvUpdatePlayerChip;
      _Listener = null;
    }

    private void Update()
    {
      _Timer += Time.deltaTime;
      if (_Timer >= MAX_TIME_TO_TICK)
      {
        _Timer -= MAX_TIME_TO_TICK;
        _CurrentTime += UPDATE_TIME_AT_MS;
        _SpawnManager.SpawnFishAt(_CurrentTime);
        if (_CurrentTime >= RESET_ROOM_T)
        {
          _CurrentTime = 0;
          // reset room
        }
      }
    }

    private void OnDestroy()
    {
      Cleanup();
    }

    private void RoomInit(PlayerDetail[] players, uint roomTime, uint fishID, uint[] destroyed)
    {
      _CurrentTime = roomTime;
      for (int seat = 0; seat < players.Length; seat++)
      {
        if (players[seat].user_id <= 0) continue;
        if (players[seat].user_id == Game.GetInstance()._UserID)
        {
          _TurretPosition.SetClientTurret(seat, _FishGameUIListener);
        }
        int turretDataIndex = 0;
        _TurretPosition.SetTurretBySeat(seat, ResourcesHelper.GetTurretDataFromIndex(turretDataIndex));
        _TurretPosition.UpdatePlayerChip(seat, players[seat].chip);
      }
      _MainGameUI.SetupChangeTurretButton(_FishGameUIListener);
      _SpawnManager.SetupData(fishID, destroyed);
    }

    private void RecvFishList(FishInitData[] fishes)
    {
      _SpawnManager.AddFishData(fishes);
    }

    private void RecvDestroyFish(uint seat, uint fishID, RewardAmount reward)
    {
      _SpawnManager.DestroyFish(fishID, reward);
    }

    private void RecvAdjustBet(int seat, uint bet, TurretBetState state)
    {
      _TurretPosition.UpdatePlayerBet(seat, bet, state);
    }

    private void RecvChangeTurret(int seat,int turretDataindex)
    {
      _TurretPosition.SetTurretBySeat(seat, ResourcesHelper.GetTurretDataFromIndex(turretDataindex));
    }

    private void RecvUpdatePlayerChip(uint seat, ulong chip)
    {
      _TurretPosition.UpdatePlayerChip((int)seat, chip);
    }
  }
}
