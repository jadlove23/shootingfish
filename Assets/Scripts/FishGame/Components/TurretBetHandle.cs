using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public struct TurretBet
{
  public int Bet;
  public TurretBetState TurretBetState;
  public TurretBet(int bet, TurretBetState turretBetState)
  {
    Bet = bet;
    TurretBetState = turretBetState;
  }

}
public struct TurretBetState
{
  public bool MinusActive;
  public bool PlusActive;
  public TurretBetState(bool minusActive, bool plusActive)
  {
    MinusActive = minusActive;
    PlusActive = plusActive;
  }
}

public class TurretBetHandle
{
  private ulong _ClientBet = 10;
  private ulong _ClientChip;
  private TurretBetState _ClientState;

  private const ulong MIN_BET = 10;
  private const ulong MAX_BET = 5000000;
  private const int PREFER_DIGITS_BET_PER_CHIP = 2;

  public void CalculateAdjustBet(int operation)
  {
    if (operation != 1 && operation != -1) return;
    long diff = RaisedToPowerDigits(GetPowerFromValue(_ClientBet), operation);
    UpdateTurretBet((ulong)diff);
  }

  public void CalculatePreferBet(ulong chip)
  {
    int preferDigit = GetPowerFromValue(chip) - PREFER_DIGITS_BET_PER_CHIP;
    int firstDigit = (int)(chip / Math.Pow(10, GetPowerFromValue(chip)));
    _ClientBet = (ulong)(Mathf.Pow(10, preferDigit) * firstDigit);
    UpdateBetState();
  }

  public ulong Getbet()
  {
    return _ClientBet;
  }

  public void UpdateChip(ulong chip)
  {
    _ClientChip = chip;
  }

  public TurretBetState GetTurretBetState()
  {
    return _ClientState;
  }

  private void UpdateTurretBet(ulong diff)
  {
    _ClientBet += diff;
    UpdateBetState();
  }

  private void UpdateBetState()
  {
    _ClientBet = (ulong)Mathf.Clamp(_ClientBet, MIN_BET, MAX_BET);
    _ClientState.MinusActive = _ClientBet > MIN_BET;
    _ClientState.PlusActive = _ClientBet < MAX_BET && CanAddMoreBet();
  }

  private int GetPowerFromValue(ulong val)
  {
    return (int)Mathf.Floor(Mathf.Log10(val));
  }

  private long RaisedToPowerDigits(int power, int operation)
  {
    long diff = (long)(Mathf.Pow(10, power) * operation);
    bool isDiffEqualNegativeBet = diff.Equals(-(long)_ClientBet);
    if (isDiffEqualNegativeBet) return diff / 10;  // remove 1 digits
    return diff;
  }

  private bool CanAddMoreBet()
  {
    ulong diff = (ulong)RaisedToPowerDigits(GetPowerFromValue(_ClientBet), 1);
    return _ClientBet + diff <= _ClientChip;
  }

}
