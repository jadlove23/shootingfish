﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace FishGame
{
  public class Fish : MonoBehaviour, IPoolObject
  {
    [SerializeField]
    public FishData ScriptableObject = default;

    [SerializeField]
    private Transform[] _FishBodyPositions = default;

    [SerializeField]
    private FishMovement _Movement;

    public FishMovement GetFishMovement() { return _Movement; }

    private System.Action _OnEnd;

    public void OnObjectSpawn()
    {
      if (FishData.ReferenceEquals(ScriptableObject, null)) return;
      _Movement.Move(() =>
      {
        ReturnToPool();
      });
    }

    public void AddFishToFishHolderPosition(GameObject[] fishes)
    {
      for (int i = 0; i < fishes.Length; i++)
      {
        if (fishes[i] == null) continue;
        fishes[i].transform.SetParent(_FishBodyPositions[i], false);
        fishes[i].transform.localPosition = Vector3.zero;
      }
    }

    public void SetCurrentTime(float elapseTime, float lifeTime)
    {
      _Movement.SetCurrentTime(elapseTime, lifeTime);
    }

    public void SetOnEnd(System.Action onEnd)
    {
      _OnEnd = onEnd;
    }

    public void SetMovementPattern(int pattern, float xOffset = 0f, float yOffset = 0f)
    {
      SetOffsetPosition(pattern, xOffset, yOffset);
      _Movement.SetMovementPattern(pattern);
    }

    public float GetTimeToMove()
    {
      return _Movement.GetTimeToMove();
    }

    public void ReturnToPool()
    {
      System.Action onEnd = _OnEnd;
      _OnEnd = null;
      for (int i = 0; i < _FishBodyPositions.Length; i++)
      {
        FishBody fishBody = _FishBodyPositions[i].GetComponentInChildren<FishBody>();
        if (fishBody != null)
          fishBody.ReturnToFishPool();
      }
      ObjectPooler.ReturnToPool(gameObject, PoolType.OBJECT_POOL, onEnd);
    }

    private void SetOffsetPosition(int pattern, float xOffset, float yOffset)
    {
      switch (pattern)
      {
        case 0:
        case 1:
          transform.position = new Vector3(xOffset, transform.position.y, transform.position.z);
          return;
        case 2:
        case 3:
          transform.position = new Vector3(transform.position.x, yOffset, transform.position.z);
          return;
      }
    }
  }
}
