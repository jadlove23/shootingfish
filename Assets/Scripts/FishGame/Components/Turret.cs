﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace FishGame
{
  public class Turret : MonoBehaviour
  {
    [Header("Player Setting")]
    [SerializeField]
    private bool _IsClientControl = false;
    [SerializeField]
    private Transform _FirePoint;
    [SerializeField]
    private float _FireRate = 0.5f;
    [SerializeField]
    private GameObject _AdjustBetButtons = default;
    [SerializeField]
    private Button _Minus = default;
    [SerializeField]
    private Button _Plus = default;
    [SerializeField]
    private Text _BetText = default;
    [SerializeField]
    private Text _ChipText = default;

    private float _NextFire = 0f;
    private uint _Bet = 1;
    private float _Angle;
    private TurretData _TurretData;
    private IFishGameUIListener _FishGameUIListener;

    private const float DEFAULT_Z_DEGREE = -90;
    private const float MIN_DISTANCE_TO_IGNORE = 23f;

    private void Awake()
    {
      _Minus.onClick.AddListener(() => _FishGameUIListener.AdjustBet(-1));
      _Plus.onClick.AddListener(() => _FishGameUIListener.AdjustBet(1));
    }

    private void Update()
    {
      if (_IsClientControl) 
        PlayerShooting();
    }

    public void SetTurret(TurretData turretData)
    {
      _TurretData = turretData;
      _FireRate = _TurretData.FireRate;
      transform.GetChild(2).GetComponent<Image>().sprite = _TurretData.Sprite;
    }

    private void PlayerShooting()
    {
      PlayerTurretRotation();
      if (Input.GetButton("Fire1") && Time.time > _NextFire && !EventSystem.current.IsPointerOverGameObject())
      {
        ShootingBullet();
      }
    }

    private void PlayerTurretRotation()
    {
      if (CheckRotateTurret()) return;
      Vector2 direction = Input.mousePosition - _FirePoint.position;
      float zDegree = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
      float turretRotation = (zDegree > 0 && zDegree < 180) ? zDegree : ClosestBetween(Mathf.Abs(zDegree), 0, 180);   
      transform.rotation = Quaternion.Euler(0f, 0f, turretRotation + DEFAULT_Z_DEGREE);
    }

    private bool CheckRotateTurret()
    {
      Vector3 view = Camera.main.ScreenToViewportPoint(Input.mousePosition);
      bool isOutside = view.x < 0 || view.x > 1 || view.y < 0 || view.y > 1;
      float distance = Vector3.Distance(Input.mousePosition, _FirePoint.position);
      return isOutside || distance < MIN_DISTANCE_TO_IGNORE;
    }

    private float ClosestBetween(float val, float low, float high)
    {
      if (val > low && val < high)
      {
        float mid = (high - low) / 2 + low;
        return val < mid ? low : high;
      }
      return val;
    }

    private void ShootingBullet()
    {
      //NetworkHelper.SendPlayerShootBullet(MainGameController.GetPlayerManager().GetPlayerDataByID(_ID));
      if (!Game.GetInstance().GetFishRoom().ClientFire()) return;
      _NextFire = !_IsClientControl ? Time.time + _FireRate * Random.Range(2f, 5f) : Time.time + _FireRate;
      Vector2 firePos = Camera.main.ScreenToWorldPoint(_FirePoint.position);
      GameObject bullet = ObjectPooler.SpawnObjectFromPool("Bullet", firePos, transform.rotation * Camera.main.transform.rotation);
      bullet.GetComponent<BulletScript>().SetupBet(true, 1);
      SoundManager.PlaySFX(SoundManager.SFX.Turret_Shooting);
      NetworkHelper.SendPlayerFire(0, _Bet, (uint)Mathf.Round(_Angle));
    }

    public void SetClientControl(bool active, IFishGameUIListener fishGameUIListener)
    {
      _IsClientControl = active;
      _AdjustBetButtons.SetActive(active);
      _FishGameUIListener = fishGameUIListener;
    }

    public void SetCurrentBet(uint bet)
    {
      _Bet = bet;
      _BetText.text = $"x{bet}";
    }

    public void SetBetState(TurretBetState betState)
    {
      _Minus.interactable = betState.MinusActive;
      _Plus.interactable = betState.PlusActive;
    }

    public void SetCurrentChip(ulong chip)
    {
      _ChipText.text = chip.ToString("N0");
    }
  }
}