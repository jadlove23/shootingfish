using FishGame;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourcesHelper
{
  public static TurretData GetTurretDataFromIndex(int index)
  {
    TurretData turretType = Resources.Load<TurretData>($"Scriptable/Turrets/Turret_{index}");
    if (turretType == null) return null;
    return turretType;
  }
}
