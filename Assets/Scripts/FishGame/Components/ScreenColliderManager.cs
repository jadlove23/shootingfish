using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenColliderManager : MonoBehaviour
{
  GameObject top;
  GameObject bottom;
  GameObject left;
  GameObject right;

  void Awake()
  {
    top = new GameObject("Top");
    bottom = new GameObject("Bottom");
    left = new GameObject("Left");
    right = new GameObject("Right");
  }

  void Start()
  {
    CreateScreenColliders();
  }

  private void Update()
  {
#if UNITY_EDITOR
    CreateScreenColliders();
#endif
  }

  void CreateScreenColliders()
  {
    Vector3 bottomLeftScreenPoint = Camera.main.ScreenToWorldPoint(new Vector3(0f, 0f, 0f));
    Vector3 topRightScreenPoint = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, 0f));
    BoxCollider2D collider = top.GetComponent<BoxCollider2D>();
    if (collider == null)
      collider = top.AddComponent<BoxCollider2D>();
    collider.size = new Vector3(Mathf.Abs(bottomLeftScreenPoint.x - topRightScreenPoint.x), 0.5f, 0f);
    top.transform.position = new Vector3(0, topRightScreenPoint.y, 0f);
    top.tag = "Wall";
    top.transform.SetParent(this.transform);

    collider = bottom.GetComponent<BoxCollider2D>();
    if (collider == null)
      collider = bottom.AddComponent<BoxCollider2D>();
    collider.size = new Vector3(Mathf.Abs(bottomLeftScreenPoint.x - topRightScreenPoint.x), 0.5f, 0f);
    bottom.transform.position = new Vector3(0, bottomLeftScreenPoint.y, 0f);
    bottom.tag = "Wall";
    bottom.transform.SetParent(this.transform);

    collider = left.GetComponent<BoxCollider2D>();
    if (collider == null)
      collider = left.AddComponent<BoxCollider2D>();
    collider.size = new Vector3(0.5f, Mathf.Abs(topRightScreenPoint.y - bottomLeftScreenPoint.y), 0f);
    left.transform.position = new Vector3( bottomLeftScreenPoint.x, 0, 0f);
    left.tag = "Wall";
    left.transform.SetParent(this.transform);

    collider = right.GetComponent<BoxCollider2D>();
    if (collider == null)
      collider = right.AddComponent<BoxCollider2D>();
    collider.size = new Vector3(0.5f, Mathf.Abs(topRightScreenPoint.y - bottomLeftScreenPoint.y), 0f);
    right.transform.position = new Vector3(topRightScreenPoint.x, 0, 0f);
    right.tag = "Wall";
    right.transform.SetParent(this.transform);
  }
}
