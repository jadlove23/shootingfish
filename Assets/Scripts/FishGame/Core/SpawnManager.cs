﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace FishGame
{
  public enum FishFormation
  {
    SINGLE,
    STRAIGHT_LINE,
    T_SHAPE,
  }
  public class FishSpawnData
  {
    public TypeOfFish type;
    public FishFormation formation;
    public uint amount;

    public FishSpawnData(TypeOfFish type, FishFormation formation, uint amount)
    {
      this.type = type;
      this.formation = formation;
      this.amount = amount;
    }
  }
  public class SpawnManager
  {
    private uint _CurrentId = 0;
    private List<FishInitData> _FishSpawn = new List<FishInitData>();
    private HashSet<uint> _DestroyFishID;

    private Dictionary<uint, FishBody> _FishOnScene = new Dictionary<uint, FishBody>();

    private Dictionary<uint, FishSpawnData> _FishTypeAmount = new Dictionary<uint, FishSpawnData>()
    {
      { 1, new FishSpawnData(TypeOfFish.SMALL_FISH, FishFormation.STRAIGHT_LINE, 3)},
      { 2, new FishSpawnData(TypeOfFish.SMALL_FISH, FishFormation.T_SHAPE, 16)},
      { 3, new FishSpawnData(TypeOfFish.BLUE_FISH, FishFormation.SINGLE, 1)},
      { 4, new FishSpawnData(TypeOfFish.RED_FISH, FishFormation.SINGLE, 1)},
      { 5, new FishSpawnData(TypeOfFish.PURPLE_FISH, FishFormation.SINGLE, 1)},
      { 6, new FishSpawnData(TypeOfFish.SQUID, FishFormation.SINGLE, 1)},
    };

    public void SetupData(uint id, uint[] destroyFishID)
    {
      _CurrentId = id;
      _DestroyFishID = new HashSet<uint>(destroyFishID);
    }

    public void AddFishData(FishInitData[] fishes)
    {
      _FishSpawn.AddRange(fishes);
    }

    public void SpawnFishAt(uint currentTime)
    {
      while (_FishSpawn.Count > 0)
      {
        FishInitData data = _FishSpawn[0];
        if (currentTime >= data.init_t && (data.life_time + data.init_t) >= currentTime)
        {
          if (_FishTypeAmount.ContainsKey(data.fish_type))
          {
            FishSpawnData fishSpawnData = _FishTypeAmount[data.fish_type];
            uint time = currentTime - data.init_t;
            GameObject fishHolder = ObjectPooler.SpawnFishHolderFromPool(fishSpawnData.formation, data, time);
            GameObject[] fishes = new GameObject[fishSpawnData.amount];
            for (int i = 0; i < fishSpawnData.amount; i++)
            {
              if (!_DestroyFishID.Contains(_CurrentId))
              {
                GameObject go = ObjectPooler.SpawnFishFromPool(_CurrentId, fishSpawnData.type, () => RemoveFish(_CurrentId));
                _FishOnScene.Add(_CurrentId, go.GetComponent<FishBody>());
                fishes[i] = go;
                _CurrentId++;
              }
            }
            fishHolder.GetComponent<Fish>().AddFishToFishHolderPosition(fishes);
          }
          else Debug.Log($"Fish {data.fish_type} dont exits");

          _FishSpawn.RemoveAt(0);
        }
        else break;
      }
    }

    public void DestroyFish(uint id, RewardAmount reward)
    {
      _DestroyFishID.Add(id);
      if (!_FishOnScene.ContainsKey(id)) return;
      _FishOnScene[id].Destroy(reward);
      RemoveFish(id);
    }

    private void RemoveFish(uint id)
    {
      if (!_FishOnScene.ContainsKey(id)) return;
      _FishOnScene.Remove(id);
    }
  }
}
