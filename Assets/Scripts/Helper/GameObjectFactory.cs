﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FishGame;
using System;
using UnityEngine.UI;

public class GameObjectFactory : MonoBehaviour
{
  public static GameObject NewLobbyRoom(Transform parent, string name, SeatData[] seat, uint roomID)
  {
    GameObject obj = Instantiate(Resources.Load("Prefabs/Lobby/Roomlist"), parent) as GameObject;
    obj.GetComponent<LobbyRoomEntry>().Setup(name, seat, roomID);
    return obj;
  }

  public static GameObject NewLobby(Transform parent)
  {
    GameObject obj = Instantiate(Resources.Load("Prefabs/Lobby/Lobby"), parent) as GameObject;
    return obj;
  }

  public static GameObject InstantiateUI(string name, string path)
  {
    var obj = new GameObject();
    Canvas c = obj.AddComponent<Canvas>();
    c.renderMode = RenderMode.ScreenSpaceOverlay;
    obj.AddComponent<CanvasScaler>();
    obj.AddComponent<GraphicRaycaster>();
    InstantiateGameObject(path, obj.transform).name = name;
    obj.name = "Canvas" + name;
    return obj;
  }

  private static GameObject InstantiateGameObject(string path, Transform transform)
  {
    GameObject obj = Instantiate(Resources.Load($"Prefabs/FishGame/UI/{path}"), transform) as GameObject;
    return obj;
  }
}
