using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FishGame
{
  public class FishBody : MonoBehaviour
  {
    [SerializeField]
    private GameObject _ParticleEffect;
    [SerializeField]
    private GameObject _FloatingText;
    [SerializeField]
    private Color _FlashColor = Color.red;

    private System.Action<uint, uint> _OnHit;

    private BoxCollider2D _BoxCollider;
    private uint _FishID;
    private bool _IsAlive = false;
    private Animator _Animator;
    private SpriteRenderer[] _Renderers;

    private System.Action _OnEnd;
    private Coroutine _FlashTask;

    private const string TAG_BULLET = "Bullet";
    private const float RESET_HIT_COLOR = 0.125f;
    private const float TIME_TO_DESTROY = 1.5f;
    private readonly Color DEFAULT_COLOR = Color.white;

    private void Awake()
    {
      _Animator = GetComponent<Animator>();
      _BoxCollider = GetComponent<BoxCollider2D>();
      _Renderers = GetComponentsInChildren<SpriteRenderer>();
    }

    private void OnEnable()
    {
      SetRendererColor(DEFAULT_COLOR);
    }

    public void SetOnEnd(System.Action onEnd)
    {
      _OnEnd = onEnd;
    }

    public void SetFishId(uint fishID)
    {
      _FishID = fishID;
      _IsAlive = true;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
      if (collision.gameObject.CompareTag(TAG_BULLET) && _IsAlive)
      {
        SoundManager.PlaySFX(SoundManager.SFX.Hiting_Fish);
        BulletScript bullet = collision.gameObject.GetComponent<BulletScript>();
        ObjectPooler.ReturnToPool(collision.gameObject);
        if (_FlashTask != null)
        {
          StopCoroutine(_FlashTask);
          _FlashTask = null;
        }
        _FlashTask = StartCoroutine(Flash(_FlashColor));
        if (bullet.IsMine()) NetworkHelper.SendHitFish(_FishID, bullet.GetBet());
      }
    }

    public void Destroy(RewardAmount reward)
    {
      _IsAlive = false;
      transform.SetParent(null);
      _BoxCollider.enabled = false;
      _Animator.SetTrigger("dead");
      GameObject floatingText = Instantiate(_FloatingText, this.transform.position, Camera.main.transform.rotation);
      floatingText.transform.Find("FloatingIncome").GetComponent<TextMesh>().text = reward.amount.ToString();
      GameObject particleEffect = Instantiate(_ParticleEffect, this.transform.position, Quaternion.identity);
      Destroy(floatingText, TIME_TO_DESTROY);
      Destroy(particleEffect, TIME_TO_DESTROY);
    }

    public void ReturnToFishPool()
    {
      System.Action onEnd = _OnEnd;
      _OnEnd = null;
      _BoxCollider.enabled = true;
      ObjectPooler.ReturnToPool(gameObject, PoolType.FISH_POOL, onEnd);
    }

    private IEnumerator Flash(Color color)
    {
      SetRendererColor(color);
      yield return new WaitForSeconds(RESET_HIT_COLOR);
      SetRendererColor(DEFAULT_COLOR);
    }

    private void SetRendererColor(Color color)
    {
      foreach (var rd in _Renderers)
      {
        rd.color = color;
      }
    }
  }

}