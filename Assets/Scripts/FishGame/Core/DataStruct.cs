﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FishGame
{
  public enum TypeOfFish
  {
    SMALL_FISH,
    BLUE_FISH,
    RED_FISH,
    PURPLE_FISH,
    SQUID,
    // note: add more here
  }

  public struct PlayerData
  {
    public int Id;
    public float Money;
    public string Name;
    public int Bet;

    public PlayerData(int id, float money, string name, int bet)
    {
      Id = id;
      Money = money;
      Name = name;
      Bet = bet;
    }
  }

  public struct SeatData
  {
    public bool IsNotEmpty;
    public PlayerData Player;
    public uint SeatNumber;
    public bool IsBot;
    public SeatData(bool isNotEmpty, PlayerData playerData, uint seat, bool isBot)
    {
      IsNotEmpty = isNotEmpty;
      Player = playerData;
      SeatNumber = seat;
      IsBot = isBot;
    }
  }

  public struct RoomData
  {
    public uint Id;
    public string Name;
    public SeatData[] Seats;
    public uint Seed;
    public HashSet<uint> FishDestroy;
    public RoomData(uint id, string name, uint seed)
    {
      Id = id;
      Name = name;
      Seats = new SeatData[4];
      Seed = seed;
      FishDestroy = new HashSet<uint>();
    }
  }
}
