﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LobbySceneController : MonoBehaviour
{
  [SerializeField]
  private Transform _Canvas;

  private void Awake()
  {
    NetworkHelper.SendOnSceneLobby();
    SoundManager.PlayBGM(SoundManager.BGM.Lobby);
    SetUp();
  }
  public void SetUp()
  {
    GameObjectFactory.NewLobby(_Canvas);
  }

}
