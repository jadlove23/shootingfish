using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FishGame;

public static class FishConst
{
  public static readonly Dictionary<uint, FishSpawnData> FISH_SPAWN_DATA = new Dictionary<uint, FishSpawnData>()
  {
    { 1, new FishSpawnData(TypeOfFish.SMALL_FISH, FishFormation.STRAIGHT_LINE, 3)},
    { 2, new FishSpawnData(TypeOfFish.SMALL_FISH, FishFormation.T_SHAPE, 16)},
    { 3, new FishSpawnData(TypeOfFish.BLUE_FISH, FishFormation.SINGLE, 1)},
    { 4, new FishSpawnData(TypeOfFish.RED_FISH, FishFormation.SINGLE, 1)},
    { 5, new FishSpawnData(TypeOfFish.PURPLE_FISH, FishFormation.SINGLE, 1)},
    { 6, new FishSpawnData(TypeOfFish.SQUID, FishFormation.SINGLE, 1)},
  };
}
