﻿using FishGame;
public class DelegateReceiver 
{
  public delegate void DelegateLobbyScene();
  public DelegateLobbyScene OnLobbyScene;
  public delegate void DelegateUserData(ulong chip);
  public DelegateUserData OnUpdateChip;
  public delegate void DelegateFishGameChipEntries(FishRoomChipEntry[] chip_entries);
  public DelegateFishGameChipEntries OnFishGameChipEntries;
  public delegate void DelegateSceneFishGame(uint room_id, uint[] bets);
  public DelegateSceneFishGame OnSceneFishGame;
  public delegate void DelegateFishGameRoomSetup(PlayerDetail[] players, uint room_t, uint fish_id, FishInitData[] fish_list, uint[] destroyed);
  public DelegateFishGameRoomSetup OnFishGameRoomSetup;
  public delegate void DelegateFishGamePlayerJoin(uint seat, PlayerDetail player);
  public DelegateFishGamePlayerJoin OnFishGamePlayerJoin;
  public delegate void DelegateFishGamePlayerLeave(uint seat);
  public DelegateFishGamePlayerLeave OnFishGamePlayerLeave;
  public delegate void DelegatePlayerFire(uint seat, uint bullet_type, uint bet, uint angle);
  public DelegatePlayerFire OnPlayerFire;
  public delegate void DelegatePlayerFireLock(uint seat, uint bullet_type, uint bet, uint fish_id);
  public DelegatePlayerFireLock OnPlayerFireLock;
  public delegate void DelegateFishList(FishInitData[] fishes);
  public DelegateFishList OnFishList;
  public delegate void DelegateUpdateFishGamePlayerChip(uint seat, ulong chip);
  public DelegateUpdateFishGamePlayerChip OnUpdateFishGamePlayerChip;
  public delegate void DelegateDestroyFish(uint fish_id, uint seat, RewardAmount reward);
  public DelegateDestroyFish OnDestroyFish;
}

public class FishRoomChipEntry
{
  public uint min_chip;
  public uint min_bet;
  public uint max_bet;
}

[System.Serializable]
public class FishInitData
{
  public uint fish_type; // 1 - small fish + straight, 2 - small + T, 3 - Sliver single , 4 -Gold Single
  public uint pattern;
  public int x_offset;
  public int y_offset;
  public uint init_t;
  public uint life_time;
}

public class PlayerDetail
{
  public uint user_id;
  public ulong chip;
}

public class RewardAmount
{
  public string reward;
  public ulong amount;
}