using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FishGame
{
  public class WarningObject : MonoBehaviour
  {
    private System.Action _EndEvent;
    // Start is called before the first frame update
    public void SetEndEvent(System.Action endEvent)
    {
      _EndEvent = endEvent;
    }

    public void DoEnd()
    {
      _EndEvent?.Invoke();
    }
  }
}
