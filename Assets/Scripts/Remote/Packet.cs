﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FishGame;

public class Packet 
{
  private static DelegateReceiver  _Receiver;

  public static void Setup(DelegateReceiver  receiver)
  {
    _Receiver = receiver;
  }

  public static void SendUpdateChip(ulong chip)
  { // update when enter lobby scene
    _Receiver.OnUpdateChip?.Invoke(chip);
  }

  public static void SendLobbyScene()
  {
    _Receiver.OnLobbyScene?.Invoke();
  }

  public static void SendFishDestroyID(uint fishId, uint seat, RewardAmount reward)
  {
    _Receiver.OnDestroyFish?.Invoke(fishId, seat, reward);
  }

  public static void SendFishGameScene(uint roomId, uint[] bets)
  {
    _Receiver.OnSceneFishGame?.Invoke(roomId, bets);
  }

  public static void SendFishGameRoomSetup(PlayerDetail[] players, uint room_t, uint fish_id, FishInitData[] fish_list, uint[] destroyed)
  {
    _Receiver.OnFishGameRoomSetup?.Invoke(players, room_t, fish_id, fish_list, destroyed);
  }

  public static void SendPlayerJoin(uint seat, PlayerDetail player)
  {
    _Receiver.OnFishGamePlayerJoin?.Invoke(seat, player);
  }

  public static void SendPlayerLeave(uint seat)
  {
    _Receiver.OnFishGamePlayerLeave?.Invoke(seat);
  }

  public static void SendFishList(FishInitData[] fishes)
  {
    _Receiver.OnFishList?.Invoke(fishes);
  }

  public static void SendPlayerFire(uint seat, uint bullet_type, uint bet, uint angle)
  {
    _Receiver.OnPlayerFire?.Invoke(seat, bullet_type, bet, angle);
  }

  public static void SendPlayerFireLock(uint seat, uint bullet_type, uint bet, uint fishId)
  {
    _Receiver.OnPlayerFireLock?.Invoke(seat, bullet_type, bet, fishId);
  }

  public static void SendPlayerUpdateChip(uint seat, ulong chip)
  {
    _Receiver.OnUpdateFishGamePlayerChip?.Invoke(seat, chip);
  }
}