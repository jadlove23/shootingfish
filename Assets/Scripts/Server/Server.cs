﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FishGame;

[System.Serializable]
public class FishHitRate
{
  public TypeOfFish type;
  public float hitRate;
  public float reward;
}

public class Server : MonoBehaviour
{
  [SerializeField]
  private FishInitData[] _FishData = default;

  [SerializeField]
  private FishHitRate[] _FishHitData = default;

  private static Server _Instance = null;
  public static Server GetInstance() { return _Instance; }
  private ServerDelegateReceiver _Receiver = new ServerDelegateReceiver();

  private int _Seat = NON_PLAYING_SEAT;
  private PlayerDetail _ClientDetail = new PlayerDetail() { user_id = 1, chip = 1000 };

  private ServerRoom _Room;
  private const int NON_PLAYING_SEAT = -1;

  private uint _CurrentTime;
  private float _Timer = 0f;
  private const float MAX_TIME_TO_TICK = 10f;
  private const uint UPDATE_TIME_AT_MS = 10000;
  private const uint RESET_ROOM_T = 300000; // 5 minutes

  void Awake()
  {
    if (_Instance == null || _Instance == this)
    {
      _Instance = this;
      DontDestroyOnLoad(this);
    }
    else
    {
      Destroy(this.gameObject);
    }
    NetworkHelper.Setup(_Receiver);
    SetupRecv();
    _Room = new ServerRoom(1, _FishData, _FishHitData);
  }

  private void Update()
  {
    _Timer += Time.deltaTime;
    if (_Timer >= MAX_TIME_TO_TICK)
    {
      _Timer -= MAX_TIME_TO_TICK;
      _CurrentTime += UPDATE_TIME_AT_MS;
      _Room.SendFish();
      if (_CurrentTime >= RESET_ROOM_T)
      {
        _CurrentTime = 0;
        // reset room
      }
    }
  }

  private void SetupRecv()
  {
    _Receiver.OnRecvJoinFishGame += RecvFishGamePlayerJoin;
    _Receiver.OnRecvFishGameLeave += RecvPlayerLeave;
    _Receiver.OnRecvPlayerFire += RecvPlayerFire;
    _Receiver.OnRecvPlayerFireLock += RecvPlayerFireLock;
    _Receiver.OnRecvHitFish += RecvHitFish;
  }

  public void PlayerJoinSuccess()
  {
    _Room.PlayerJoinSuccess();
  }

  private void RecvFishGamePlayerJoin(uint index)
  {
    if (index > ServerRoom.MAX_PLAYER_SEAT) return;
    _Seat = (int)index;
    _Room.PlayerJoin(index, _ClientDetail);
  }

  private void RecvPlayerLeave()
  {
    if (!IsClientPlaying()) return;
    _Room.PlayerLeave((uint)_Seat);
    _Seat = NON_PLAYING_SEAT;
    Packet.SendLobbyScene();
  }

  private void RecvPlayerFire(uint bulletType, uint bet, uint angle)
  {
    if (!IsClientPlaying()) return;
    _Room.PlayerFire((uint)_Seat, bet);
  }

  private void RecvPlayerFireLock(uint bulletType, uint bet, uint fishId)
  {
    if (!IsClientPlaying()) return;
  }

  private void RecvHitFish(uint fishId, uint bet)
  {
    if (!IsClientPlaying()) return;
    _Room.HitFish((uint)_Seat, fishId, bet);
  }

  private bool IsClientPlaying()
  {
    return _Seat >= 0;
  }
}
