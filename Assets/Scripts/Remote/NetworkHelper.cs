﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FishGame;
public class NetworkHelper
{
  private static ServerDelegateReceiver _Receiver;

  public static void Setup(ServerDelegateReceiver receiver)
  {
    _Receiver = receiver;
  }
  public static void SendPlayerUpdate(string roomId, PlayerData playerdata)
  {
    _Receiver.OnRecvPlayerUpdate?.Invoke(roomId, playerdata); 
  }

  public static void SendAdjustBet(int bet, PlayerData playerdata)
  {
    playerdata.Bet += bet;
    //SendPlayerUpdate(Game.GetInstance().GetRoomData().GetRoomID(), playerdata);
  }

  public static int SendAddIncome(int income, PlayerData playerdata)
  {
    int Income = income * playerdata.Bet;
    playerdata.Money += Income;
    //SendPlayerUpdate(Game.GetInstance().GetRoomData().GetRoomID(), playerdata);
    return Income;
  }

  public static void SendPlayerShootBullet(PlayerData playerdata)
  {
    playerdata.Money -= 1 * playerdata.Bet;
    //SendPlayerUpdate(Game.GetInstance().GetRoomData().GetRoomID(), playerdata);
  }

  public static void SendFishID(string id, uint roomID)
  {
    _Receiver.OnRecvFishDestroy?.Invoke(id, roomID);
  }

  public static void SendPlayerSit(string roomId,uint seat)
  {
    _Receiver.OnRecvPlayerSit?.Invoke(roomId, seat);
  }

  public static void SendPlayerLeaveRoom(RoomData room, uint seat)
  {
    _Receiver.OnRecvPlayerLeaveRoom?.Invoke(room, seat);
  }

  public static void SendOnSceneLobby()
  {
    _Receiver.OnRecvOnSceneLobby?.Invoke();
  }

  public static void SendJoinFishGame(uint index)
  {
    _Receiver.OnRecvJoinFishGame(index);
  }

  public static void SendFishGameLeave()
  {
    _Receiver.OnRecvFishGameLeave();
  }

  public static void SendPlayerFire(uint bulletType, uint bet, uint angle)
  {
    _Receiver.OnRecvPlayerFire(bulletType, bet, angle);
  }

  public static void SendPlayerFireLock(uint bulletType, uint bet, uint fishId)
  {
    _Receiver.OnRecvPlayerFireLock(bulletType, bet, fishId);
  }

  public static void SendHitFish(uint fishId, uint bet)
  {
    _Receiver.OnRecvHitFish(fishId, bet);
  }
}
