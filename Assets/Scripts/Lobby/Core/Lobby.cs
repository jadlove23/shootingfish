﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FishGame;

public class Lobby 
{
  private List<RoomData> _Rooms  = new List<RoomData>() { new RoomData(1, "hello", 1) };

  public List<RoomData> GetRooms()
  {
    return _Rooms;
  }
}
