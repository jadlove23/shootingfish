﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using FishGame;
public class LobbyRoomEntry : MonoBehaviour
{
  [SerializeField]
  private Button[] _SeatButton = new Button[4];
  public void Setup(string name, SeatData[] seat, uint roomId)
  {
    transform.Find("Name").GetComponent<Text>().text = name;
    for (int i = 0; i < seat.Length; i++)
    {
      if (seat[i].IsNotEmpty)
        _SeatButton[i].GetComponent<Image>().color = Color.red;
      else
        _SeatButton[i].GetComponent<Image>().color = Color.green;
      _SeatButton[i].interactable = !seat[i].IsNotEmpty;
    }
    _SeatButton[0].onClick.AddListener(() => Sit(0, roomId));
    _SeatButton[1].onClick.AddListener(() => Sit(1, roomId));
    _SeatButton[2].onClick.AddListener(() => Sit(2, roomId));
    _SeatButton[3].onClick.AddListener(() => Sit(3, roomId));
  }

  private void Sit(uint seat, uint roomId)
  {
    //NetworkHelper.SendPlayerSit(roomId, seat);
    NetworkHelper.SendJoinFishGame(seat);
  }
}
