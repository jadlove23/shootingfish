using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager
{
  public enum BGM
  {
    Lobby,
    Maingame,
  }

  public enum SFX
  {
    Turret_Shooting,
    Hiting_Fish,
  }

  private static string[] _SfxPath =
  {
    "Turret_Shooting",
    "Fish_Hit",
  };

  private static string[] _BGMPath =
{
    "BGM_Lobby",
    "BGM_MainGame"
  };

  private static Dictionary<string, AudioClip> _AudioClipList = new Dictionary<string, AudioClip>();
  private static AudioSource _AudioSource;

  public static void LoadAllAudioFromResource()
  {
    var AudioClips = Resources.LoadAll("Sound", typeof(AudioClip));
    foreach (AudioClip audioClip in AudioClips)
    {
      if (!_AudioClipList.ContainsKey(audioClip.name))
        _AudioClipList.Add(audioClip.name, audioClip);
    }
  }

  public static void LoadAudioClip(string path, System.Action<AudioClip> callback, bool isOneShot = true)
  {
    if (_AudioClipList.ContainsKey(path)) callback(_AudioClipList[path]);
    else Debug.LogWarning($"Audio not found {path}");
  }

  public static void PlaySFX(SFX fx, bool dontDestroyOnLoad = false)
  {
    LoadAudioClip(_SfxPath[(int)fx], (clip) =>
    {
      if (clip == null)
      {
        Debug.LogWarning($"SFX load failed, got null from: {fx}");
        return;
      }
      PlayOneShotAudioClip(clip, dontDestroyOnLoad);
    });
  }

  public static void PlayBGM(BGM bgm)
  {
    LoadAudioClip(_BGMPath[(int)bgm], (clip) =>
    {
      if (clip == null)
      {
        Debug.LogWarning($"BGM load failed, got null from: {bgm}");
        return;
      }
      PlayBGMAudioClip(clip);
    }, false);
  }

  private static AudioSource PlayOneShotAudioClip(AudioClip clip, bool dontDestroyOnLoad)
  {
    _AudioSource = GameObject.FindObjectOfType<AudioSource>();
    if (_AudioSource == null)
      _AudioSource = new GameObject("Oneshot", typeof(AudioSource)).GetComponent<AudioSource>();
    if (clip == null)
    {
      Debug.LogError("PlayBGMAudioClip : clip not found.");
    }
    else
    {
      _AudioSource.PlayOneShot(clip);
    }
    return _AudioSource;
  }

  private static AudioSource PlayBGMAudioClip(AudioClip clip)
  {
    _AudioSource = GameObject.FindObjectOfType<AudioSource>();
    if (_AudioSource == null)
      _AudioSource = new GameObject("Oneshot", typeof(AudioSource)).GetComponent<AudioSource>();
    if (clip == null)
    {
      Debug.LogError("PlayBGMAudioClip : clip not found.");
    }
    else
    {
      _AudioSource.clip = clip;
    }
    _AudioSource.loop = true;
    _AudioSource.Play();
    return _AudioSource;
  }

}
