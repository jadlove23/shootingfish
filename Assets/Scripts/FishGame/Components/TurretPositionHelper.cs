using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FishGame
{
  public class TurretPositionHelper 
  {
    private int _ClientPos = 0;
    private readonly Dictionary<int, int> _SeatIndex = new Dictionary<int, int>() { { 0, 0 }, { 1, 1 }, { 2, 2 }, { 3, 3 } };
    private readonly Dictionary<int, int> _InvertSeatIndex = new Dictionary<int, int>() { { 0, 3 }, { 1, 2 }, { 2, 1 }, { 3, 0 } };

    public void Setup(int pos)
    {
      _ClientPos = pos;
    }
    public int GetSeatIndexFromSeat(int seat)
    {
      if (!_SeatIndex.ContainsKey(seat)) return seat;
      return (_ClientPos == 2 || _ClientPos == 3)? _InvertSeatIndex[seat] : _SeatIndex[seat];
    }

  }
}
