﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FishGame
{
  public class TurretPosition : MonoBehaviour
  {
    [SerializeField]
    private Turret[] _Turrets = new Turret[4];
    private TurretPositionHelper _TurretPositionHelper = new TurretPositionHelper();
    public void SetClientTurret(int seat, IFishGameUIListener fishGameUIListener)
    {
      _TurretPositionHelper.Setup(seat);
      CheckIsTopSeat(seat);
      int index = _TurretPositionHelper.GetSeatIndexFromSeat(seat);
      _Turrets[index].SetClientControl(true, fishGameUIListener);
    }

    public void UpdatePlayerBet(int seat, uint bet, TurretBetState state)
    {
      int index = _TurretPositionHelper.GetSeatIndexFromSeat(seat);
      _Turrets[index].SetCurrentBet(bet);
      _Turrets[index].SetBetState(state);

    }

    public void UpdatePlayerChip(int seat, ulong chip)
    {
      int index = _TurretPositionHelper.GetSeatIndexFromSeat(seat);
      _Turrets[index].SetCurrentChip(chip);
    }

    public void CheckIsTopSeat(int seat)
    {
      bool isTopSeat = false;
      if (seat == 2 || seat == 3)
      {
        isTopSeat = true;
      }
      float zDegree = isTopSeat ? 180 : 0;
      Camera.main.transform.Rotate(new Vector3(0, 0, zDegree));
    }

    public void SetTurretBySeat(int seat, TurretData TurretDataindex)
    {
      int index = _TurretPositionHelper.GetSeatIndexFromSeat(seat);
      _Turrets[index].SetTurret(TurretDataindex);
    }
  }
}