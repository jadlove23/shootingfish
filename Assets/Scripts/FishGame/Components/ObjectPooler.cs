﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace FishGame
{
  public interface IPoolObject
  {
    void OnObjectSpawn();
  }
  public enum PoolType
  {
    OBJECT_POOL,
    FISH_POOL
  }

  public class ObjectPooler : MonoBehaviour
  {
    [System.Serializable]
    public class ObjectPool
    {
      public string tag;
      public GameObject prefab;
      public int size;
    }

    [System.Serializable]
    public class FishPool
    {
      public TypeOfFish type;
      public GameObject prefab;
      public int size;
    }

    [System.Serializable]
    public class FishOffset
    {
      public List<Transform> transforms;

      public Vector3 GetOffsetPosition(int offset)
      {
        if (transforms.Count <= 0) return Vector3.zero;
        int mid = (transforms.Count / 2) + 1;
        int index = mid + offset;
        if (index < 0 || index >= transforms.Count) return Vector3.zero;
        return transforms[index].position;
      }
    }

    [SerializeField]
    private List<ObjectPool> _ObjectPools;
    [SerializeField]
    private List<FishPool> _FishPools;
    [SerializeField]
    private FishOffset _XOffsetRef;
    [SerializeField]
    private FishOffset _YOffsetRef;
    [SerializeField]
    private Transform _ObjectPoolParent;
    [SerializeField]
    private Transform _FishPoolParent;

    private static Transform _FishParent;
    private static FishOffset _XOffset;
    private static FishOffset _YOffset;
    private static Dictionary<string, Queue<GameObject>> _ObjectPoolDictionary;
    private static Dictionary<TypeOfFish, Queue<GameObject>> _FishPoolDictionary;

    private void Awake()
    {
      SetupObjectPool(_ObjectPools, _ObjectPoolParent);
      SetupFishPool(_FishPools, _FishPoolParent);
      _FishParent = _FishPoolParent;
      _XOffset = _XOffsetRef;
      _YOffset = _YOffsetRef;
    }

    private static void SetupObjectPool(List<ObjectPool> objectPools, Transform parent)
    {
      _ObjectPoolDictionary = new Dictionary<string, Queue<GameObject>>();
      foreach (ObjectPool pool in objectPools)
      {
        Queue<GameObject> poolQueue = new Queue<GameObject>();
        for (int i = 0; i < pool.size; i++)
        {
          GameObject obj = Instantiate(pool.prefab, parent);
          obj.name = obj.name + i;
          obj.SetActive(false);
          obj.transform.position = Vector3.zero;
          poolQueue.Enqueue(obj);
        }
        _ObjectPoolDictionary.Add(pool.tag, poolQueue);
      }
    }

    private static void SetupFishPool(List<FishPool> fishPools, Transform parent)
    {
      _FishPoolDictionary = new Dictionary<TypeOfFish, Queue<GameObject>>();
      foreach (FishPool pool in fishPools)
      {
        Queue<GameObject> poolQueue = new Queue<GameObject>();
        for (int i = 0; i < pool.size; i++)
        {
          GameObject obj = Instantiate(pool.prefab, parent);
          obj.name = obj.name + i;
          obj.SetActive(false);
          obj.transform.position = Vector3.zero;
          poolQueue.Enqueue(obj);
        }
        _FishPoolDictionary.Add(pool.type, poolQueue);
      }
    }

    public static GameObject SpawnObjectFromPool(string tag, Vector3 postition, Quaternion rotation)
    {
      if (!_ObjectPoolDictionary.ContainsKey(tag))
      {
        Debug.Log("Pool with tag" + tag + "donsn't exist.");
        return null;
      }
      GameObject objectToSpawn = _ObjectPoolDictionary[tag].Dequeue();
      if (objectToSpawn != null)
      {
        objectToSpawn.SetActive(true);
        objectToSpawn.transform.position = postition;
        objectToSpawn.transform.rotation = rotation;
        IPoolObject pooledObj = objectToSpawn.GetComponent<IPoolObject>();
        if (pooledObj != null)
        {
          pooledObj.OnObjectSpawn();
        }
        _ObjectPoolDictionary[tag].Enqueue(objectToSpawn);
      }
      return objectToSpawn;
    }

    public static GameObject SpawnFishHolderFromPool(FishFormation formation, FishInitData data, uint timelapse, System.Action onEnd = null)
    {
      GameObject objectToSpawn = _ObjectPoolDictionary[$"{formation}"].Dequeue();
      if (objectToSpawn != null)
      {
        objectToSpawn.SetActive(true);
        float x = _XOffset.GetOffsetPosition(data.x_offset).x;
        float y = _YOffset.GetOffsetPosition(data.y_offset).y;
        Fish fishHolder = objectToSpawn.GetComponent<Fish>();
        fishHolder.SetMovementPattern((int)data.pattern, x, y);
        fishHolder.SetCurrentTime(timelapse, data.life_time);
        fishHolder.SetOnEnd(onEnd);

        IPoolObject pooledObj = objectToSpawn.GetComponent<IPoolObject>();
        if (pooledObj != null)
        {
          pooledObj.OnObjectSpawn();
        }
        _ObjectPoolDictionary[$"{formation}"].Enqueue(objectToSpawn);
      }
      return objectToSpawn;
    }

    public static GameObject SpawnFishFromPool(uint currentID, TypeOfFish fishType, System.Action onEnd = null)
    { 
      TypeOfFish type = fishType;
      if (!_FishPoolDictionary.ContainsKey(type))
      {
        Debug.Log("Fish Pool with type" + type.ToString() + "donsn't exist.");
        return null;
      }

      GameObject objectToSpawn = _FishPoolDictionary[type].Dequeue();
      if (objectToSpawn != null)
      {
        objectToSpawn.SetActive(true);
        FishBody fish = objectToSpawn.GetComponent<FishBody>();
        fish.SetFishId(currentID);
        fish.SetOnEnd(onEnd);

        IPoolObject pooledObj = fish.GetComponent<IPoolObject>();
        if (pooledObj != null)
        {
          pooledObj.OnObjectSpawn();
        }
        _FishPoolDictionary[type].Enqueue(objectToSpawn);
      }
      return objectToSpawn;
    }

    public static void ReturnToPool(GameObject obj, PoolType type = PoolType.OBJECT_POOL, System.Action onReturnToPool = null)
    {
      Debug.Log(obj.name + " return to pool");
      if(type == PoolType.FISH_POOL)
        obj.transform.SetParent(_FishParent, false);
      obj.transform.position = Vector3.zero;
      obj.transform.rotation = Quaternion.identity;
      obj.SetActive(false);
      onReturnToPool?.Invoke();
    }
  }
}
