using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace FishGame
{
  [CreateAssetMenu(fileName = "TurretData", menuName = "ScriptableOjects/TurretScriptableObjects", order = 1)]
  public class TurretData : ScriptableObject
  {
    [SerializeField]
    public float FireRate;
    [SerializeField]
    public Sprite Sprite;
  }

}