﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FishGame
{
  public class BulletScript : MonoBehaviour, IPoolObject
  {
    [SerializeField]
    private float _Speed = 10f;

    [Header("Refect Bullet")]
    [SerializeField]
    private int _BounceNumber = 2;
    [SerializeField]
    private bool _IsBouncing;

    private Rigidbody2D _RB;
    private int _Bounce;
    private Vector3 _Direction;
    private Vector3 _LastVelocity;

    private bool _IsMine = false;

    private uint _Bet = 1;
    public uint GetBet() { return _Bet; }

    private void Awake()
    {
      _RB = GetComponent<Rigidbody2D>();
    }

    public void OnObjectSpawn()
    {
      _Bounce = _BounceNumber;
      _Direction = transform.up;
    }

    private void FixedUpdate()
    {
      _RB.velocity = _Direction * _Speed;
    }

    private void LateUpdate()
    {
      _LastVelocity = _RB.velocity;
    }

    public bool IsMine() { return _IsMine; }

    public void SetupBet(bool isMine, uint bet)
    {
      _IsMine = isMine;
      _Bet = bet;
    }

    public void HitFish(uint fishId)
    {
      if (!_IsMine) return;
      NetworkHelper.SendHitFish(fishId, _Bet);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
      if (collision.gameObject.CompareTag("Wall"))
      {
        if (_IsBouncing && _Bounce > 0)
        {
          _Bounce--;
          Vector3 _wallNormal = collision.GetContact(0).normal;
          _Direction = Vector3.Reflect(_Direction, _wallNormal);
          Quaternion bulletRotation = Quaternion.FromToRotation(_LastVelocity, _Direction) * transform.rotation;
          transform.rotation = bulletRotation;
        }
        else
        {
          ObjectPooler.ReturnToPool(gameObject);
        }
      }
    }

  }
}
