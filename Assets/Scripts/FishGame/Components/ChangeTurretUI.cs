using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeTurretUI : MonoBehaviour
{
  private IFishGameUIListener _FishGameUIListener;
  [SerializeField]
  private Button _TurretType1 = default;
  [SerializeField]
  private Button _TurretType2 = default;
  [SerializeField]
  private Button _TurretType3 = default;
  [SerializeField]
  private Button _CloseButton = default;

  private void Awake()
  {
    _TurretType1.onClick.AddListener(() => _FishGameUIListener.ChangeTurret(0));
    _TurretType2.onClick.AddListener(() => _FishGameUIListener.ChangeTurret(1));
    _TurretType3.onClick.AddListener(() => _FishGameUIListener.ChangeTurret(2));
    _CloseButton.onClick.AddListener(() => Close());
  }

  public void Setup(IFishGameUIListener fishGameUIListener)
  {
    _FishGameUIListener = fishGameUIListener;
  }
  
  public void Close()
  {
    Destroy(transform.root.gameObject);
  }
}
