﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using FishGame;

public class LobbyUI : MonoBehaviour
{
  [SerializeField]
  private Transform _RoomListContent;
  private List<GameObject> _RoomList = new List<GameObject>();
  private void Start()
  {
    CreatLobbyRoomList(Game.GetInstance().GetLobbyData().GetRooms());
  }

  private void CreatLobbyRoomList(List<RoomData> rooms)
  {
    if (rooms.Count == 0) return;
    for (int i = 0; i < rooms.Count; i++)
    {
      _RoomList.Add(GameObjectFactory.NewLobbyRoom(_RoomListContent, rooms[i].Name, rooms[i].Seats, rooms[i].Id));
    }
  }
}
