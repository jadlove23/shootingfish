﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InitSceneController : MonoBehaviour
{
  // Start is called before the first frame update
  void Start()
  {
    StartCoroutine("WaitLoading");
  }
  IEnumerator WaitLoading()
  {
    yield return new WaitForSeconds(2f);
    SceneManager.LoadScene("Lobby");
  }
}
